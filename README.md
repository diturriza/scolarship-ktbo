KTBO Test
===================


Prueba realizada por Daniel Iturriza

----------


Test Requirements
-------------
[Requerimientos.txt](https://bitbucket.org/diturriza/scolarship-ktbo/src/bc15141a7705?at=master)

Tecnologias utilizadas
-------------------

 - Sails Js, framework basado en Nodejs para realizar webapps.
 - Mongodb, base de datos no relacional
 - Jquery, libreria basada en javascript para realizar las solicitudes de datos desde el cliente.
 - Twitter Bootstrap, libreria de estilo base para la aplicacion.
 - Chart.js, libreria basada en javascript para realizar excelentes graficas.

Pre-Requisitos
-------------------

 - Instalar mongodb
 - Instalar Nodejs & npm

Instalacion
-------------------

    git clone https://diturriza@bitbucket.org/diturriza/scolarship-ktbo.git

    cd scolarship-ktbo
    npm install
    sails lift

Flujo de datos
-------------------

 1. En inicio, la aplicacion no mostrara solo datos. Ingrese datos del estudiante.
 2. Enseguida a la derecha de la pantalla la grafica con los datos almacenados.