$(document).ready(function() {
  getCharts();
})

function calculate() {
  var data = {};

  data.dni = $('#dni').val();
  data.average = $('#average').val();
  data.payment = $('#payment').val();

  console.log(data);
  $.ajax({
    url: '/students/',
    type: 'POST',
    data: data,
    success: function(result) {
      getCharts();
      alert("DNI: " + result.dni +"\nAverage: " + result.average + "\nNew Payment By Grant: " + result.paymentByGrant + "\nOld Payment: " + result.payment);
    },
    error: function(error) {
      alert(error);
    }
  });
}

function getCharts() {
  $.get('/students/resume/', function(data) {
    var ctx = $("#myChart").get(0).getContext("2d");
    console.log(data);
    var options = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke: true,

      //String - The colour of each segment stroke
      segmentStrokeColor: "#fff",

      //Number - The width of each segment stroke
      segmentStrokeWidth: 2,

      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts

      //Number - Amount of animation steps
      animationSteps: 100,

      //String - Animation easing effect
      animationEasing: "easeOutBounce",

      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate: true,

      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale: false,

      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"

    }
    var myChart = new Chart(ctx).Pie(data, options);
    $('ul').html('');
    _.forEach(data, function (elem) {
    	$('ul').append('<li style="color: '+elem.color+'">'+elem.label+': '+elem.value+'</li>')
    });

  })
    .fail(function(error) {
      alert(error);
    });

}