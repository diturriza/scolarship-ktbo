/**
 * StudentController
 *
 * @description :: Server-side logic for managing students
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  resume: function(req, res) {
    Student.find({}, function(err, students) {
      if (err) return res.badRequest(err);

      var studentsAverages = _.pluck(_.filter(students, 'average'), 'average');
      var a=0,b=0,c=0,d=0,e=0,f=0,g=0,h=0;

      _.forEach(studentsAverages, function (n) {
        if (n == 10) 
          a += 1;

        if (n >= 9.6 && n < 10) 
          b += 1;

        if (n >= 9.2 && n < 9.6);
          c += 1

        if (n >= 9 && n < 9.2);
          d += 1

        if (n >= 8.8 && n < 9)
          e += 1

        if (n >= 8.4 && n < 8.8)
          f += 1

        if (n >= 8 && n < 8.4)
          g += 1

        if (n < 8)
          h += 1
      });

      var data = [{
        value: a,
        color: "#4CAF50",
        highlight: "#81C784",
        label: "Grant of 100%"
      }, {
        value: b,
        color: "#CDDC39",
        highlight: "#DCE775",
        label: "Grant of 61%"
      }, {
        value: c,
        color: "#FFC107",
        highlight: "#FFD54F",
        label: "Grant of 37.3%"
      }, {
        value: d,
        color: "#FF5722",
        highlight: "#FF8A65",
        label: "Grant of 30%"
      }, {
        value: e,
        color: "#673AB7",
        highlight: "#9575CD",
        label: "Grant of 25.1%"
      }, {
        value: f,
        color: "#2196F3",
        highlight: "#64B5F6",
        label: "Grant of 20.6%"
      },{
        value: g,
        color: "#2196F3",
        highlight: "#64B5F6",
        label: "Grant of 20%"
      }, {
        value: h,
        color: "#F44336",
        highlight: "#E57373",
        label: "Others"
      }];
      return res.ok(data);
    })
  }

};