/**
* Student.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	dni :{
  		type: 'string'
  	},
    average: {
      type: 'integer',
      min: 0,
      max: 10
    },
    payment: {
      type: 'float',
      min: 0
    },
    paymentByGrant: {
      type: 'float',
      min: 0
    }

  },
  
  beforeCreate: function(values, cb){
    var grant, average = values.average;

    if(average == 10) 
      grant = sails.config.grants.A;
    else if (average >= 9.6)
      grant = sails.config.grants.B;
    else if (average >= 9.2)
      grant = sails.config.grants.C;
    else if (average >= 9)
      grant = sails.config.grants.D;
    else if (average >= 8.8)
      grant = sails.config.grants.E;
    else if (average >= 8.4)
      grant = sails.config.grants.F;
    else if (average >= 8)
      grant = sails.config.grants.G;
    else 
      grant = 0;
    
    values.paymentByGrant = values.payment - (values.payment * grant);

    cb();
  }
};

